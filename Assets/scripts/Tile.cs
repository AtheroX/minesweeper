﻿/*
* Copyright (c) AtheroX
* https://twitter.com/athero_x
*/

using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class Tile: MonoBehaviour, IPointerClickHandler {
	
	#region Variables
		public GameManager gameManager;

		public int id;
		public int bombsNear;
		public bool imClicked = false;
		public bool imABomb = false;
		public bool tengoBandera = false;
		public Vector2 tileOutlineSize;
	#endregion

	#region UnityMethods

	void Start() {
		GetComponent<Outline>().effectDistance = tileOutlineSize;
	}
	public void OnPointerClick(PointerEventData eventData) {
		//Debug.Log(eventData +" "+ eventData.button);

		if (eventData.button == PointerEventData.InputButton.Left) {
			if (GetComponent<Button>().interactable&&!imClicked) {
				imClicked = true;
				if (gameManager.Playing) {
					if (imABomb) {
						GetComponent<Image>().sprite = gameManager.imagenes[9];
						gameManager.LoseGame(gameObject);
					} else {
						if (bombsNear > 0) {
							GetComponent<Image>().sprite = gameManager.imagenes[bombsNear];
						} else {
							GetComponent<Image>().sprite = gameManager.imagenes[10];
						}
						gameManager.Turn(gameObject,true);
					}
				}
			}
		} else if (eventData.button == PointerEventData.InputButton.Right) {
			if (GetComponent<Button>().interactable && !imClicked) {
				tengoBandera = !tengoBandera;
				if (tengoBandera) {
					GetComponent<Image>().sprite = gameManager.imagenes[12];
				}
				else {
					GetComponent<Image>().sprite = gameManager.imagenes[11];
				}
				gameManager.Turn(gameObject, false);
			}
		}
	}
		

	public void CheckBombs() {
		bombsNear = gameManager.CheckForBombs(gameObject);
	}

	public void FakeClick() {
		if (GetComponent<Button>().interactable && !imClicked) {
			imClicked = true;
			if (gameManager.Playing) {
				if (imABomb) {
					GetComponent<Image>().sprite = gameManager.imagenes[9];
					gameManager.LoseGame(gameObject);
				}
				else {
					if (bombsNear > 0) {
						GetComponent<Image>().sprite = gameManager.imagenes[bombsNear];
					}
					else {
						GetComponent<Image>().sprite = gameManager.imagenes[10];
					}
					gameManager.Turn(gameObject, true);
				}
			}
		}
	}
	#endregion

}
